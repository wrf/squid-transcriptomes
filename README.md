# Transcriptomes of luminous squid #

[Transcriptomes of luminous squid and other molluscs to identify genes involved in bioluminescence and novel bioluminescence chemistries for medical and imaging applications, and studies of evolution of bioluminescence](https://www.ncbi.nlm.nih.gov/bioproject/342927)

Data for paper:  Francis, WR, LM Christianson, SHD Haddock (2017) [Symplectin evolved from multiple duplications in bioluminescent squid](https://peerj.com/articles/3633/). *PeerJ* 5:e3633

![symplectin_raxml_tree_metazoans_v1.png](https://bitbucket.org/repo/kMMXEMr/images/317184400-symplectin_raxml_tree_metazoans_v1.png)

### Files in the [source tab](https://bitbucket.org/wrf/squid-transcriptomes/src) ###

* Fasta-format Alignment of symplectin, homologs, and all outgroups
* RAxML output files (trees and logs) for symplectin alignment
* PDB-format model of symplectin based on [human vanin-1](http://www.rcsb.org/pdb/explore/explore.do?structureId=4CYF) generated using the [HHPred webserver](https://toolkit.tuebingen.mpg.de/#/)

### Transcriptomic data in [downloads tab](https://bitbucket.org/wrf/squid-transcriptomes/downloads) ###
* [Chiroteuthis calyx](https://www.ncbi.nlm.nih.gov/biosample/5787345) arms -- assembly contains [78445 transcripts](https://bitbucket.org/wrf/squid-transcriptomes/downloads/chiroteuthis_calyx_trinity.fasta.gz), raw reads at [SRR5527417](https://www.ncbi.nlm.nih.gov/sra/SRX2700332)
* [Dosidicus gigas](https://www.ncbi.nlm.nih.gov/biosample/5787346) photophores -- assembly contains [81554 transcripts](https://bitbucket.org/wrf/squid-transcriptomes/downloads/dosidicus_gigas_trinity_norm.fasta.gz), raw reads at [SRR5152122](https://www.ncbi.nlm.nih.gov/sra/SRX2464239)
* [Octopoteuthis deletron](https://www.ncbi.nlm.nih.gov/biosample/5787343) arms -- assembly contains [122672 transcripts](https://bitbucket.org/wrf/squid-transcriptomes/downloads/octopoteuthis_deletron_trinity.fasta.gz), raw reads at [SRR5527415](https://www.ncbi.nlm.nih.gov/sra/SRX2700330)
* [Phylliroe bucephala](https://www.ncbi.nlm.nih.gov/biosample/5787342) (not cephalopod) whole animal -- assembly contains [66535 transcripts](https://bitbucket.org/wrf/squid-transcriptomes/downloads/phylliroe_bucephala_trinity.fasta.gz), raw reads at [SRR5527414](https://www.ncbi.nlm.nih.gov/sra/SRX2700334)
* [Pterygioteuthis hoylei](https://www.ncbi.nlm.nih.gov/biosample/5787347) photophores -- assembly contains [93201 transcripts](https://bitbucket.org/wrf/squid-transcriptomes/downloads/pterygioteuthis_hoylei_trinity.fasta.gz), raw reads at [SRR5527418](https://www.ncbi.nlm.nih.gov/sra/SRX2700333)
* [Vampyroteuthis infernalis](https://www.ncbi.nlm.nih.gov/biosample/5787344) arms -- assembly contains [149961 transcripts](https://bitbucket.org/wrf/squid-transcriptomes/downloads/vampyroteuthis_trinity.fasta.gz), raw reads at [SRR5527416](https://www.ncbi.nlm.nih.gov/sra/SRX2700331)

### Assembled transcriptomes of public data ###
* Uroteuthis edulis and Euprymna scolopes, from [PRJNA257113](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA257113), by [Pankey et al 2014](http://www.pnas.org/content/111/44/E4736)
* Loligo vulgaris, from [SRR3472303](https://www.ncbi.nlm.nih.gov/sra/SRX1739671), by [Jung et al 2016](http://www.pnas.org/content/113/23/6478)
* Doryteuthis pealeii, from [PRJNA255916](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA255916), by [Alon et al 2015](https://elifesciences.org/content/4/e05198)
* Sepia pharaonis, from [SRR3011300](https://www.ncbi.nlm.nih.gov/sra/SRX1484119), by [Wen et al 2016](http://www.sciencedirect.com/science/article/pii/S1874778716300538)

### Other cephalopod resources ###
* Watasenia scintillans [transcriptome from arms and mantle](https://www.ncbi.nlm.nih.gov/nuccore/GEDZ00000000.1), by [Gimenez et al 2016](http://www.nature.com/articles/srep27638)
* Octopus vulgaris [nervous tissue transcriptome](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA79361), by [Zhang et al 2012](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0040320)
* Octopus bimaculoides [genome and gene models](https://groups.oist.jp/molgenu/octopus-genome), by [Albertin et al 2015](http://www.nature.com/nature/journal/v524/n7564/full/nature14668.html)
* [Transcriptomes](http://www.tau.ac.il/~elieis/squid/) of Nautilus pompilius, Sepia officinalis, and Octopus vulgaris, by [Liscovitch-Brauer et al 2017](http://www.cell.com/cell/abstract/S0092-8674(17)30344-6)
* Octopus maya [reproductive regulation](https://www.ncbi.nlm.nih.gov/Traces/wgs/GFAI01), by [PRJNA345483](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA345483)
* [Transcriptomes](https://www.ncbi.nlm.nih.gov/nuccore?term=337893%5BBioProject%5D) of [Sepioloidea lineolata](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GEXF01), [Euprymna tasmanica](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GEXE01), [Idiosepius notoides](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GFNE01), [Hapalochlaena maculosa](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GEXH01), and [Octopus kaurna](https://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GEXG01), by [Caruana et al 2016](https://www.ncbi.nlm.nih.gov/pubmed/27476034)